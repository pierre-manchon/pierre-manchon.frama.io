function bonjourmonde() {
    // Print text to talk about security measures
	console.log("\n%cAlright, We'll Call It A Draw\n\n\uD83D\uDD0E for beautiful things in the internet ?\nhttps://www.reddit.com/r/Eyebleach/\n\uD83D\uDD0E to contact me securely ?\nhttps://pierre-manchon.pm/~/pierre-manchon.asc\n\uD83D\uDD0E for informations on how I approach the security of my website ?\nhttps://pierre-manchon.pm/.well-known/security.txt.\n", "color: gray;");
};

// Execute functions on window load
// https://stackoverflow.com/a/688199
if (window.addEventListener) // W3C standard
{
    window.addEventListener('load', bonjourmonde, false); // NB **not** 'onload'
} else if (window.attachEvent) // Microsoft
{
    window.attachEvent('onload', bonjourmonde);
};
